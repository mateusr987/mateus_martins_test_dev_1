package backend

import java.text.DecimalFormat
import java.time.LocalDateTime

class Stock {
    Double price
    LocalDateTime priceDate
    static belongsTo = [company: Company]
    static constraints = {
        price min: 0D
    }

    String toString() {
        DecimalFormat decimalFormat = new DecimalFormat("#.00");
        return "Company Name: ${company.name}\tPrice Date: ${priceDate}\tPrice: ${decimalFormat.format(price)}"
    }
}
