package backend

import java.time.LocalDateTime

class BootStrap {

    def init = { servletContext ->
        // I tried saving new companies and stocks in two Services, but for some reason the Grails was not recognizing the Service Class's Methods. Probably some misconfiguration on my computer in the package definition.

        // Creating 3 companies
        Company ford = new Company(name: 'FORD', segment: 'vehicles')
        ford.save()
        Company fiat = new Company(name: 'FIAT', segment: 'vehicles')
        fiat.save()
        Company consul = new Company(name: 'CONSUL', segment: 'eletrodomestics')
        consul.save()

        List companies = [ ford, fiat, consul ] // list with the 3 companies
        List companyPrices = [10.00D, 12.31D, 22.13D] // list with the 3 stock prices for each company

        LocalDateTime now = LocalDateTime.now().withSecond(0).withNano(0) // gets the actual time from the LocalDateTime and sets seconds and nanoseconds to 0
        LocalDateTime startTime = now.minusDays(30).withHour(0).withMinute(0) // stores the start of the interval, 30 days ago at midnight

        // This logic is based on time going backwards, it keeps creating entries until it goes all the way back to the start of the interval
        while(now > startTime) { // While `now` is still ahead of the start of the interval do:
            while( now.hour > 9 ) { // While `now` hour is greater than 9 ( it's after 10AM ) do:
                companies.eachWithIndex{ company, index -> // For each company in `companies`, get the `company` and the respective `index` and:
                    companyPrices[index] = ( companyPrices[index] - 2 + 4 * Math.random() ) // Update the price using the index as reference adding a value between -2 and 2
                    if ( companyPrices[index] < 0 ) companyPrices[index] = 0 // Force stock price to always be greater or equal to 0
                    (new Stock(company: company, price: companyPrices[index], priceDate: now)).save() // Saves the new Stock in the database
                }
                now = now.minusMinutes(1) // After doing all 3 companies, reduce 1 minute
            }
            now = now.minusDays(1).withHour(18).withMinute(0) // After completing the day by reaching 9:59 AM, go to previous day at 6PM and start over
        }

    }
    def destroy = {
    }
}
