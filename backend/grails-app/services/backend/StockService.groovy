package backend

import grails.gorm.transactions.Transactional


import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

@Transactional
class StockService {

    List getStocks(company, numberOfHoursUntilNow){
        LocalDateTime startTime = LocalDateTime.now() // Starts timer at the beggining of the method
        def stocks = Stock.findAllByCompanyAndPriceDateGreaterThan(company, startTime.minusHours(numberOfHoursUntilNow)) // retrieve all stocks from a single company that are from less than `numberOfHoursUntilNow` ago
        String out = '' // to avoid printing multiple times, the output will be generated in this object, allowing a single print afterwards
        // given the task, it would be possible to keep printing each stock at a time to give a sense of continuity, but I chose to prioritize the efficiency of the method
        stocks.each { stock -> // for each stock found
            out += "${stock}\n" // add it to the output stream
        }
        out += "${stocks.size()} quotes retrieved\n" // add how many stocks were retrieved to the output
        print out // prints the whole output in one go
        LocalDateTime endTime = LocalDateTime.now() // ends timer
        long milliseconds = ChronoUnit.MILLIS.between(startTime, endTime); // calculates how many milliseconds the method took to complete
        println "${milliseconds} milliseconds to run" // prints the time necessary to execute
    }

}
