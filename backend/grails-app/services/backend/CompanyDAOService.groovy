package backend

import grails.gorm.transactions.Transactional

@Transactional
class CompanyDAOService {

    // returns all Company entries in the database
    def findAll() {
        return Company.findAll()
    }
}
