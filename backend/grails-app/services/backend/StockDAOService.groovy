package backend

import grails.gorm.transactions.Transactional

@Transactional
class StockDAOService {

    // returns all Stock entries in the database related to a single company
    def getByCompany( company ) {
        return Stock.findAllByCompany( company )
    }
}
