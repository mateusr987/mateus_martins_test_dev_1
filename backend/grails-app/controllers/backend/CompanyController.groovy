package backend

import util.Util
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping('/company')
class CompanyController {
    @Autowired
    private StockDAOService stockDAOService

    @Autowired
    private CompanyDAOService companyDAOService

    def index() { }

    @CrossOrigin(origins = "http://localhost:4200") // Allow Cross Origin Requests from Angular default port 4200
    @GetMapping("/standardDeviation") // Defines endpoint
    def getAllCompanyStandardDeviation(){
        List response = [] // Creates a list that'll be used in the request response
        Util util = new Util() // Instantiates object of Util class that contains getStandardDeviation() method
        companyDAOService.findAll().each { company -> // For each company in the database do: ( having each one as `company` )
            List stockPrices = stockDAOService.getByCompany(company).price // Get all stock prices related to a single company and stores it in `stockPrices`
            response.add( [name: company.name, segment: company.segment, standardDeviation: util.getStandardDeviation(stockPrices)] ) // Add the object containing the data of one company to the response list
        }
        return ResponseEntity.ok( response ) // Returns the list with a HTTP response status code 200
    }
}
