package util

class Util {

    // Calculates the Standard Deviation of given List
    Double getStandardDeviation( data ){
        Integer dataLength = data.size(); // stores list's length
        Double dataMean = data.sum() / dataLength; // calculates average value inside list
        Double sumValueMinusMeanSquared = 0D; // initialize the summatory value with a Double 0, a neutral number
        data.each { value -> // For each values inside the list:
            sumValueMinusMeanSquared += Math.pow( (value - dataMean) as Double, 2D ); // Calculate ( (value - mean) ^ 2 ) and add it to the summatory
        }
        return Math.sqrt( sumValueMinusMeanSquared / dataLength ); // Standard Deviation is found by dividing it by the number of values, and getting it's square root
    }
}
