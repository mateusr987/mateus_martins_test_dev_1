/*
Task #4
Write down a simple integration test to the task #3 you did before.
No need to check all the data retrieved by the button pushing. Just a Company name would be enough !
*/
describe('Test to be fulfilled by the candidate', () => {
  it('push the button implemented on task #3 and shows the company names', () => {
      cy.visit('http://localhost:4200') // visits the home page in angular application
      cy.get('.btnCall').click() // finds and clicks the button that'll start the action
      cy.contains('FORD') // verifies the existence of the first company
      cy.contains('FIAT') // verifies the existence of the second company
      cy.contains('CONSUL') // verifies the existence of the third company
  })
})
