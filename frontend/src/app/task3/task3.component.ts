import { Component } from '@angular/core';
import { ApiService } from '../api/api.service'

@Component({
  selector: 'app-task3',
  templateUrl: './task3.component.html',
  styleUrls: ['./task3.component.scss']
})

export class Task3Component {

  public data;

  constructor(private apiService: ApiService) {}

  get() {
    this.apiService.getResponse().subscribe( (response) => {
      this.data = response;
      console.log(this.data);
    } )
  }

}
